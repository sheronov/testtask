<?php
header('Content-Type: text/html; charset=utf-8');
include_once 'wordtransform.class.php';

$word1 = 'лужа';
$word2 = 'море';
if($_GET['word1'] && $_GET['word2']) {
    $word1 = htmlspecialchars(stripslashes(trim($_GET['word1'])));
    $word2 = htmlspecialchars(stripslashes(trim($_GET['word2'])));
    $wordTransform = new WordTransform();
    $result = $wordTransform->transformation($word1,$word2,true);
    //$result = $wordTransform->transformation('лужа','море',true);
    //$result = $wordTransform->transformation('блин','река',true);
    //$result = $wordTransform->transformation('пульт','жираф',true);
}
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Трансформация слова из одного в другое по 1 замене</title>
    <style type="text/css">
        .b_center {max-width: 960px;margin: 0 auto;padding: 30px 20px;}
        blockquote {padding: 10px;background:#eee;margin: 0;}
        h1 {padding-bottom: 0;}
        form > * {display: inline-block;margin-right: 20px; max-width: 300px;padding: 2px 4px;}

    </style>
</head>
<body>
    <div class="b_center">
        <h1>Тестовое задание:</h1>
        <blockquote>
            Требуется на PHP написать программу, которая из лужи делает море.
            Например:
            лужа -> ложа -> кожа -> ... -> море
            Т.е., меняя за 1 шаг по 1 букве, нужно из слова "лужа" вывести слово "море" словарными словами и распечатать все шаги.
        </blockquote>
        <h3>Введите слова в форму и нажмите отправить:</h3>
        <form action="/" method="get">
            <input type="text" name="word1" value="<? echo $word1;?>">
            <input type="text" name="word2" value="<? echo $word2;?>">
            <button type="submit">Отправить</button>
        </form>
        <p>P.S. работает не так быстро, как хотелось бы, так как идет перебор всех возможных слов. Используется phpMorphy.</p>
        <p>P.P.S. все слова действительно существуют, но есть устаревшие. Если отключить такие слова, пропадает "кора" (важное слово для задания)</p>
        <? if($result) { ?>
        <h2>Результат:</h2>
        <p><? echo $result; ?></p>
        <? } ?>
        <p>Некоторые примеры: <a href="/?word1=море&word2=лужа">Море->Лужа</a>, <a href="/?word1=блин&word2=река">Блин->Река</a>, <a href="/?word1=жираф&word2=пульт">Жираф->Пульт</a>,</p>
    </div>
</body>
</html>
