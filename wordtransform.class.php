<?php
require_once 'phpmorphy-0.3.7/src/common.php';

class WordTransform {

    protected $dict_path = 'phpmorphy-0.3.7/dicts'; //путь к словарю
    protected $lang = 'ru_RU'; //язык
    // опции для phpMorphy
    protected $opts = array(
//        'storage' => PHPMORPHY_STORAGE_FILE, //хранение словаря в файлах
        'storage' => PHPMORPHY_STORAGE_MEM, //хранение словаря в памяти быстрее
        'predict_by_suffix' => false, //не угадывать путем отсечения префикса
        'predict_by_db' => false, //не использовать предсказание по окончанию
    );
    /* @var $morphy phpMorphy  */
    public $morphy;

    public $onlyBaseForm = true; //настройка для поиска слова стоящего в базовой форме

    const MAX_REPLACES = 50; //ограничение по замена

    protected $word_from = '';
    protected $word_to = '';
    public $steps = array(); //шаги
    public $ru_alphabet = array(); //алфавит
    public $count = 0; //длина слова
    public $words = array();//общий массив слов
    public $find = false; //флаг найдено ли слово


    public function __construct()
    {
        setlocale(LC_CTYPE, 'ru_RU.UTF-8');
        // создаем экземпляр класса phpMorphy
        try {
            $this->morphy = new phpMorphy($this->dict_path, $this->lang, $this->opts);
        } catch (phpMorphy_Exception $e) {
            die('Error occured while creating phpMorphy instance: ' . $e->getMessage());
        }

        for ($i = 224; $i <= 255; $i++) {
            //формирование ру алфавита
            $this->ru_alphabet[] = iconv('windows-1251', 'utf-8', chr($i));
        }

    }


    /*
     * Метод трансформации слова1 в слово2 с заменой по 1 букве
     */
    public function transformation($word1 = '', $word2 = '',$onlyBaseForm = true) {
        $this->onlyBaseForm = $onlyBaseForm;
        if(empty($word1) || empty($word2)) {
            return 'Нужно ввести два слова';
        }
        if(mb_strlen($word1) != mb_strlen($word2)) {
            return "Слова {$word1} и {$word2} не равны по длине";
        }
        $error_msg = array();
        if(!$this->wordInDict($word1)) {
            $error_msg[] = "Слова {$word1} нет в словаре.";
        }
        if(!$this->wordInDict($word2)) {
            $error_msg[] = "Слова {$word2} нет в словаре.";
        }
        if(!empty($error_msg)) {
            return implode(' ',$error_msg);
        }

        $this->word_from = $word1;
        $this->word_to = $word2;
        $this->count = count($this->mb_str_split($this->word_to));

        return $this->startReplacing($word1);
    }

    /*
     * Разбиение юникод страки на массив
     */
    public static function mb_str_split($string) {
        return preg_split('#(?<!^)(?!$)#u', $string);
    }

    /*
     * Фильтрация выборки для castFormByGramInfo
     * http://phpmorphy.sourceforge.net/dokuwiki/manual-graminfo
     */
     static function filterGrammems($form, $partOfSpeech, $grammems, $formN) {
        //семантические признаки
         $find = true;

         if($partOfSpeech != 'С') { //только существительные
             return false;
         }

         $needs = array('ЕД','ИМ'); //единственное число именительного падежа (можно поставить НО, для неодушевлённых, но пропадаут животные, добавятся национальности)
         if(array_diff($needs,$grammems)) {
             return false;
         }

        $semantic_indication = array(
            'МН','РД','ДТ','ВН','ТВ','ПР','ЗВ','2', //падежи и множественное число
            'ИМЯ','ФАМ','ОТЧ','ЛОК', //собственные
            'АББР','ОРГ','ВОПР','УКАЗАТ','ЖАРГ', //прочие слова
            'РАЗГ','АРХ','ОПЧ','ПОЭТ','ПРОФ','ПОЛОЖ',
//            'ДФСТ' //устаревшие? PMY_RG_DE_FACTO_SING_TANTUM
        );

        foreach ($semantic_indication as $indicator) {
            if(in_array($indicator,$grammems)) {
                $find = false;
                break;
            }
        }
        return $find;
    }


    /*
     * Проверка есть ли такое слово в словаре phpMorphy
     */
    public function wordInDict($word = '') {
        $word = mb_strtoupper($word, "UTF-8");

        $base_form = $this->morphy->castFormByGramInfo($word,null,null,1,'WordTransform::filterGrammems');
        if(false === $base_form) {
            return false;
        }

        //можно проверить на более строгое совпадение, чтобы искал только слова Базовой формы
        if($this->onlyBaseForm && !in_array($word,$base_form)) {
            return false;
        }

        return true;

    }


    /*
     * Метод для фомрирования слов перебором.
     * Не самый лучший, но рабочий
     */
    protected function startReplacing($word = '') {
        $output = 'Не найдено возможных путей';

        $levels = array();
        $this->words[$word] = 0; //схема слово -> родитель для дальнейшего поиска
        $levels[0][] = $word; //первое слово на 0 уровне
        //левел - одна итерация замены
        for ($level=0;$level<count($levels);$level++) {
            //без рекурсии динамически добавляемые уровни в массив
            $words = $levels[$level];
            foreach($words as $wk=>$word) {
                //перебор по кажлому слову
                for($i = 0; $i < $this->count;$i++) {
                    //по каждой букве
                    for($j = 0; $j < count($this->ru_alphabet);$j++) {
                        //по всем буква русского алфавита (без ё)
                        $new_word = $this->mb_str_split($word);
                        if($new_word[$i] != $this->ru_alphabet[$j]) {
                            $new_word[$i] = $this->ru_alphabet[$j];
                            $new_word = implode('', $new_word);
                            if (!in_array($new_word, array_keys($this->words))) {
                                //проверка было ли уже слово в общем массиве
                                if ($this->wordInDict($new_word)) {
                                    //проверка существует ли в словаре такое слово
                                    $levels[$level + 1][] = $new_word; //добавление на новый уровень
                                    $this->words[$new_word] = $word; //найденное слово -> родитель
                                }
                            }
                            if ($new_word == $this->word_to) {
                                //найдено слово
                                $this->find = true;
                                break 4;
                            }
                        }
                    }
                }
            }
            //ограничение по количеству замен, чтобы сервер не вешать
            if($level == self::MAX_REPLACES) {
                break;
            }
        }

        if($this->find) {
            //поиск маршрута
            if($route = array_reverse($this->findRoute($this->word_to))) {
                $output = 'Найдены замены: '.implode(' -> ',$route).'<br>';
                foreach (array_values($route) as $k => $word) {
                    $k+= 1;
                    $output.= "{$k}. {$word}<br>";
                }
            }
        }

        return $output;
    }

    /*
     * Рекусривный поиск машрута по массиву всех слов
     */
    protected function findRoute($word = '',$route = array()) {
        if(isset($this->words[$word])) {
            $route[] = $word;
            $this->steps[] = $word;
            if($this->words[$word] === 0) {
                return $route;
            } else {
                $route = array_merge($this->findRoute($this->words[$word],$route));
            }
        }

        return $route;
    }

    /*
     * DEPRECATED
     * Получить все существующие слова с одной заменой по исходному слову
     */
    protected function getOneReplaceWords($word = '',$level = 0,$exclude_words = array()) {
        $words = array();
        for ($i = 0; $i < $this->count; $i++) {
            //перебираем только по количеству букв в слове
            for ($j = 0; $j < count($this->ru_alphabet); $j++) {
                //умножаем на русский алфавит

                $orig_word = $this->mb_str_split($word); //переданное слово в массив
                if ($orig_word[$i] != $this->ru_alphabet[$j]) {
                    //если буквы разные - меняем
                    $new_word = $orig_word;
                    $new_word[$i] = $this->ru_alphabet[$j];
                    $new_word = implode('', $new_word); //снова в строку

                    if (!in_array($new_word, array_keys($this->words))) {
                        //проверка было ли слово в глобальном массиве
                        if ($this->wordInDict($new_word)) {
                            //найдено в словаре phpMoprhy
                            $this->words[$new_word] = $word; // в общий массив
                            $words[$new_word] = array(); //в текущий

                            if ($new_word == $this->word_to) {
                                echo 'Найдено слово ' . $new_word . ' на уровне ' .$level;
                                $this->find = true;
                            }
                        }
                    }
                }
            }
        }
        return $words;
    }



}